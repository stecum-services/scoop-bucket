# scoop-bucket

Scoop configs I maintain. Just because Scoop is AWSOME 😎😎😎

## Getting started

You need scoop installed, otherwise this is the place you should be looking for: [scoop.sh](https://scoop.sh)

I just added these commands to make it easier. I just condensed the information from [scoop-docs.vercel.app](https://scoop-docs.vercel.app/docs/concepts/Buckets.html#known-buckets) to quickly copy paste in your command line.

```
scoop bucket add stecum-bucket https://gitlab.com/stecum-services/scoop-bucket.git

```

## Roadmap

If I need something and is not in the extra, I'll add it. You are free to propose software, I will see what I can do.

I also will add my own software here when ready for release. 

## Authors and acknowledgment
I think this is the best way to distribute software form an enduser perspective. I hope person installing the open and free software still try to contribute to the authors by other means. 

## Contributing
Always open for others to extend this bucket. But please, don't add any software already present in the extra or core buckets already existing. I will also remove software if it is added in the 'main stream' buckets.
